﻿namespace MyChatApp_Client
{
    partial class Client
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReceivingMessage = new System.ComponentModel.BackgroundWorker();
            this.label_userIP = new System.Windows.Forms.Label();
            this.label_userPort = new System.Windows.Forms.Label();
            this.userIP = new System.Windows.Forms.TextBox();
            this.userPort = new System.Windows.Forms.TextBox();
            this.chatStory = new System.Windows.Forms.ListBox();
            this.message = new System.Windows.Forms.TextBox();
            this.Connect = new System.Windows.Forms.Button();
            this.Attach = new System.Windows.Forms.Button();
            this.label_userName = new System.Windows.Forms.Label();
            this.userName = new System.Windows.Forms.TextBox();
            this.label_serverIP = new System.Windows.Forms.Label();
            this.label_serverPort = new System.Windows.Forms.Label();
            this.serverIP = new System.Windows.Forms.TextBox();
            this.serverPort = new System.Windows.Forms.TextBox();
            this.groupBox_user = new System.Windows.Forms.GroupBox();
            this.groupBox_server = new System.Windows.Forms.GroupBox();
            this.Disconnect = new System.Windows.Forms.Button();
            this.Send = new System.Windows.Forms.Button();
            this.groupBox_user.SuspendLayout();
            this.groupBox_server.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReceivingMessage
            // 
            this.ReceivingMessage.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ReceivingMessage_DoWork);
            // 
            // label_userIP
            // 
            this.label_userIP.AutoSize = true;
            this.label_userIP.Location = new System.Drawing.Point(6, 35);
            this.label_userIP.Name = "label_userIP";
            this.label_userIP.Size = new System.Drawing.Size(17, 13);
            this.label_userIP.TabIndex = 0;
            this.label_userIP.Text = "IP";
            // 
            // label_userPort
            // 
            this.label_userPort.AutoSize = true;
            this.label_userPort.Location = new System.Drawing.Point(6, 66);
            this.label_userPort.Name = "label_userPort";
            this.label_userPort.Size = new System.Drawing.Size(26, 13);
            this.label_userPort.TabIndex = 1;
            this.label_userPort.Text = "Port";
            // 
            // userIP
            // 
            this.userIP.Location = new System.Drawing.Point(105, 32);
            this.userIP.Name = "userIP";
            this.userIP.Size = new System.Drawing.Size(100, 20);
            this.userIP.TabIndex = 2;
            // 
            // userPort
            // 
            this.userPort.Location = new System.Drawing.Point(105, 63);
            this.userPort.Name = "userPort";
            this.userPort.Size = new System.Drawing.Size(100, 20);
            this.userPort.TabIndex = 3;
            // 
            // chatStory
            // 
            this.chatStory.FormattingEnabled = true;
            this.chatStory.Location = new System.Drawing.Point(12, 173);
            this.chatStory.Name = "chatStory";
            this.chatStory.Size = new System.Drawing.Size(538, 160);
            this.chatStory.TabIndex = 5;
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(12, 339);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(375, 20);
            this.message.TabIndex = 6;
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(106, 93);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(100, 20);
            this.Connect.TabIndex = 7;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // Attach
            // 
            this.Attach.Enabled = false;
            this.Attach.Location = new System.Drawing.Point(394, 339);
            this.Attach.Name = "Attach";
            this.Attach.Size = new System.Drawing.Size(75, 20);
            this.Attach.TabIndex = 8;
            this.Attach.Text = "Attach";
            this.Attach.UseVisualStyleBackColor = true;
            this.Attach.Click += new System.EventHandler(this.Attach_Click);
            // 
            // label_userName
            // 
            this.label_userName.AutoSize = true;
            this.label_userName.Location = new System.Drawing.Point(6, 96);
            this.label_userName.Name = "label_userName";
            this.label_userName.Size = new System.Drawing.Size(35, 13);
            this.label_userName.TabIndex = 9;
            this.label_userName.Text = "Name";
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(105, 93);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(100, 20);
            this.userName.TabIndex = 10;
            // 
            // label_serverIP
            // 
            this.label_serverIP.AutoSize = true;
            this.label_serverIP.Location = new System.Drawing.Point(6, 35);
            this.label_serverIP.Name = "label_serverIP";
            this.label_serverIP.Size = new System.Drawing.Size(17, 13);
            this.label_serverIP.TabIndex = 11;
            this.label_serverIP.Text = "IP";
            // 
            // label_serverPort
            // 
            this.label_serverPort.AutoSize = true;
            this.label_serverPort.Location = new System.Drawing.Point(6, 66);
            this.label_serverPort.Name = "label_serverPort";
            this.label_serverPort.Size = new System.Drawing.Size(26, 13);
            this.label_serverPort.TabIndex = 12;
            this.label_serverPort.Text = "Port";
            // 
            // serverIP
            // 
            this.serverIP.Location = new System.Drawing.Point(106, 32);
            this.serverIP.Name = "serverIP";
            this.serverIP.Size = new System.Drawing.Size(100, 20);
            this.serverIP.TabIndex = 13;
            // 
            // serverPort
            // 
            this.serverPort.Location = new System.Drawing.Point(106, 63);
            this.serverPort.Name = "serverPort";
            this.serverPort.Size = new System.Drawing.Size(100, 20);
            this.serverPort.TabIndex = 14;
            // 
            // groupBox_user
            // 
            this.groupBox_user.Controls.Add(this.label_userIP);
            this.groupBox_user.Controls.Add(this.label_userPort);
            this.groupBox_user.Controls.Add(this.label_userName);
            this.groupBox_user.Controls.Add(this.userIP);
            this.groupBox_user.Controls.Add(this.userPort);
            this.groupBox_user.Controls.Add(this.userName);
            this.groupBox_user.Location = new System.Drawing.Point(12, 12);
            this.groupBox_user.Name = "groupBox_user";
            this.groupBox_user.Size = new System.Drawing.Size(211, 122);
            this.groupBox_user.TabIndex = 15;
            this.groupBox_user.TabStop = false;
            this.groupBox_user.Text = "User";
            // 
            // groupBox_server
            // 
            this.groupBox_server.Controls.Add(this.Disconnect);
            this.groupBox_server.Controls.Add(this.serverIP);
            this.groupBox_server.Controls.Add(this.serverPort);
            this.groupBox_server.Controls.Add(this.label_serverPort);
            this.groupBox_server.Controls.Add(this.Connect);
            this.groupBox_server.Controls.Add(this.label_serverIP);
            this.groupBox_server.Location = new System.Drawing.Point(329, 12);
            this.groupBox_server.Name = "groupBox_server";
            this.groupBox_server.Size = new System.Drawing.Size(221, 122);
            this.groupBox_server.TabIndex = 0;
            this.groupBox_server.TabStop = false;
            this.groupBox_server.Text = "Server";
            // 
            // Disconnect
            // 
            this.Disconnect.Enabled = false;
            this.Disconnect.Location = new System.Drawing.Point(9, 93);
            this.Disconnect.Name = "Disconnect";
            this.Disconnect.Size = new System.Drawing.Size(91, 20);
            this.Disconnect.TabIndex = 15;
            this.Disconnect.Text = "Disconnect";
            this.Disconnect.UseVisualStyleBackColor = true;
            this.Disconnect.Click += new System.EventHandler(this.Disconnect_Click);
            // 
            // Send
            // 
            this.Send.Enabled = false;
            this.Send.Location = new System.Drawing.Point(475, 339);
            this.Send.Name = "Send";
            this.Send.Size = new System.Drawing.Size(75, 20);
            this.Send.TabIndex = 16;
            this.Send.Text = "Send";
            this.Send.UseVisualStyleBackColor = true;
            this.Send.Click += new System.EventHandler(this.Send_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 369);
            this.Controls.Add(this.Send);
            this.Controls.Add(this.groupBox_server);
            this.Controls.Add(this.groupBox_user);
            this.Controls.Add(this.Attach);
            this.Controls.Add(this.message);
            this.Controls.Add(this.chatStory);
            this.Name = "Client";
            this.Text = "Client";
            this.groupBox_user.ResumeLayout(false);
            this.groupBox_user.PerformLayout();
            this.groupBox_server.ResumeLayout(false);
            this.groupBox_server.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker ReceivingMessage;
        private System.Windows.Forms.Label label_userIP;
        private System.Windows.Forms.Label label_userPort;
        private System.Windows.Forms.TextBox userIP;
        private System.Windows.Forms.TextBox userPort;
        private System.Windows.Forms.ListBox chatStory;
        private System.Windows.Forms.TextBox message;
        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.Button Attach;
        private System.Windows.Forms.Label label_userName;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.Label label_serverIP;
        private System.Windows.Forms.Label label_serverPort;
        private System.Windows.Forms.TextBox serverIP;
        private System.Windows.Forms.TextBox serverPort;
        private System.Windows.Forms.GroupBox groupBox_user;
        private System.Windows.Forms.GroupBox groupBox_server;
        private System.Windows.Forms.Button Send;
        private System.Windows.Forms.Button Disconnect;
    }
}

