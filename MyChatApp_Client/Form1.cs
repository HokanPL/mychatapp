﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace MyChatApp_Client
{
    public partial class Client : Form
    {
        private TcpClient clientSocket = new TcpClient();

        public Client()
        {
            InitializeComponent();

            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (var address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    userIP.Text = address.ToString();
                
            }
            userIP.Enabled = false;
            FormClosing += new FormClosingEventHandler((Object sender, FormClosingEventArgs e) => { Environment.Exit(0); });
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            try
            {
                clientSocket.Connect(serverIP.Text, int.Parse(serverPort.Text));
                
                if (clientSocket.Connected == true)
                {
                    chatStory.Items.Add("Connected to server");
                    userPort.Enabled = false;
					userName.Enabled = false;
                    Connect.Enabled = false;
                    Disconnect.Enabled = true;
                    Send.Enabled = true;
                    Attach.Enabled = true;
                }
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.Message.ToString());
            }

            ReceivingMessage.RunWorkerAsync();
        }

        private void Disconnect_Click(object sender, EventArgs e)
        {
            try
            {
                Close();
            }

            catch (Exception exception)
            {
                MessageBox.Show(exception.Message.ToString());
            }

        }

        private void Send_Click(object sender, EventArgs e)
        {
            try
            {
                NetworkStream serverStream = clientSocket.GetStream();


                byte[] outStream = Encoding.ASCII.GetBytes($"{userName.Text}: {message.Text}");
                byte[] outStreamWithTypeMark = new byte[outStream.Length + 1];
                outStream.CopyTo(outStreamWithTypeMark, 1);
                outStreamWithTypeMark[0] = (byte)'T';

                SendData(outStreamWithTypeMark, serverStream);

                message.Text = string.Empty;
            }
            catch (Exception)
            {
                MessageBox.Show("Sending failed. Try again");
            }
        }

        private void Attach_Click(object sender, EventArgs e)
        {
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "mp3 files (*.mp3)|*.mp3|bmp files (*.bmp)|*.bmp";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        string extension = string.Empty;
                        string extensionMarker = string.Empty;

                        filePath = openFileDialog.FileName;
                        extension = Path.GetExtension(filePath);

                        switch (extension)
                        {
                            case ".bmp":
                                extensionMarker = "B";
                                break;
                            case ".mp3":
                                extensionMarker = "M";
                                break;
                        }

                        var fileStream = openFileDialog.OpenFile();

                        NetworkStream serverStream = clientSocket.GetStream();
                        byte[] extensionMarkerByteArray = Encoding.ASCII.GetBytes(extensionMarker);
                        byte[] fileContentByteArray = File.ReadAllBytes(filePath);
                        byte[] outStream = new byte[extensionMarkerByteArray.Length + fileContentByteArray.Length];
                        extensionMarkerByteArray.CopyTo(outStream, 0);
                        fileContentByteArray.CopyTo(outStream, extensionMarkerByteArray.Length);
                        SendData(outStream, serverStream);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(exception.Message.ToString());
                    }
                }
            }
        }

        private void SendData(byte[] data, NetworkStream stream)
        {
            int bufferSize = 1024;

            byte[] dataLength = BitConverter.GetBytes(data.Length);

            stream.Write(dataLength, 0, 4);

            int bytesSent = 0;
            int bytesLeft = data.Length;

            while (bytesLeft > 0)
            {
                int curDataSize = Math.Min(bufferSize, bytesLeft);

                stream.Write(data, bytesSent, curDataSize);

                bytesSent += curDataSize;
                bytesLeft -= curDataSize;
            }
        }

        private byte[] GetData()
        {
            NetworkStream stream = clientSocket.GetStream();

            byte[] fileSizeBytes = new byte[4];
            int bytes = stream.Read(fileSizeBytes, 0, 4);
            int dataLength = BitConverter.ToInt32(fileSizeBytes, 0);

            int bytesLeft = dataLength;
            byte[] data = new byte[dataLength];

            int bufferSize = 1024;
            int bytesRead = 0;

            while (bytesLeft > 0)
            {
                int curDataSize = Math.Min(bufferSize, bytesLeft);
                if (clientSocket.Available < curDataSize)
                    curDataSize = clientSocket.Available;

                bytes = stream.Read(data, bytesRead, curDataSize);

                bytesRead += curDataSize;
                bytesLeft -= curDataSize;
            }

            return data;
        }

        private void ReceivingMessage_DoWork(object sender, DoWorkEventArgs e)
        {
            string path = @"C:\Users\Public\Documents\MyChatBox\" + $"{userName.Text}";
            while (clientSocket.Connected == true)
            {
                try
                {
                    byte[] inStream = GetData();
                    byte[] inStreamToParse;

                    switch (inStream[0])
                    {
                        case (byte)'T':

                            inStreamToParse = new byte[inStream.Length - 1];
                            Array.Copy(inStream, 1, inStreamToParse, 0, inStreamToParse.Length);

                            string returnData = Encoding.ASCII.GetString(inStreamToParse);

                            chatStory.Invoke(new Action(delegate ()
                            {
                                chatStory.Items.Add(returnData);
                            }));
                            break;

                        case (byte)'B':

                            inStreamToParse = new byte[inStream.Length - 1];
                            Array.Copy(inStream, 1, inStreamToParse, 0, inStreamToParse.Length);

                            Directory.CreateDirectory(path);
                            File.WriteAllBytes(path + @"\sentImage.bmp", inStreamToParse);

                            chatStory.Invoke(new Action(delegate ()
                            {
                                chatStory.Items.Add($"Someone sent an image. Saved in {path} as \'sentImage.bmp\'.");
                            }));

                            break;

                        case (byte)'M':

                            inStreamToParse = new byte[inStream.Length - 1];
                            Array.Copy(inStream, 1, inStreamToParse, 0, inStreamToParse.Length);

                            Directory.CreateDirectory(path);
                            File.WriteAllBytes(path + @"\sentAudio.mp3", inStreamToParse);

                            chatStory.Invoke(new Action(delegate ()
                            {
                                chatStory.Items.Add($"Someone sent an audio. Saved in {path} as \'sentAudio.mp3\'.");
                            }));

                            break;
                    }

                    
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message.ToString());
                }
            }
        }
    }
}
